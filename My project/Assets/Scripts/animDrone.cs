using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animDrone : MonoBehaviour
{
    private Animator m_Animator;
    private bool face = true;
    private bool scan = false;
    private bool show = false;
    // Start is called before the first frame update
    void Start()
    {
        m_Animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            Debug.Log("Launching scanning anim");
            face = false;
            show = false;
            scan = true;

            m_Animator.SetBool("IsFacingObject", face);
            m_Animator.SetBool("IsScanning", scan);
            m_Animator.SetBool("IsShowingLaser", show);
        }
    }
}
