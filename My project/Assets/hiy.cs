using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class hiy : MonoBehaviour
{
    public GameObject mine;
    public GameObject flame;

    private void Update()
    {
        if((transform.position - mine.transform.position).magnitude < 0.8)
        {
            flame.SetActive(true);
            flame.GetComponent<ParticleSystem>().Play();
        }
    }
}
